#ifndef PATH_TRACER_H
#define PATH_TRACER_H

#include <cuda_runtime.h>

void renderFrame(uchar4 *pbo, scene &world, int frameCount);

#endif //PATH_TRACER_H