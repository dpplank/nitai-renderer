#ifndef CAMERA_HANDLER_H
#define CAMERA_HANDLER_H


#include <cuda_runtime.h>
#include "structsWorld.h"
#include <thrust\random.h>

#include "cudaUtils.h"
#include "rayMaterial.h"

///NOTE::: Below diagram indicate where "pbo" starts in screen is bottom left
//        _________________
//      1|                 | 
//       |                 | 
//       |                 |
//       |                 |
//       |                 | 
//    0,0|_________________|1

////////////////////////////////
/////////CAMERA AND RAY/////////
////////////////////////////////

__host__ __device__ ray rayWithDOF(glm::vec2 resolution, float time, int x, int y, glm::vec3 eye, glm::vec3 view, glm::vec3 up, glm::vec3 right, float fov);

__host__ __device__ ray raycastFromCameraKernel(glm::vec2 resolution, float time, int x, int y, glm::vec3 eye, glm::vec3 view, glm::vec3 up, glm::vec3 right, float fovy);

__host__ __device__ ray rayWithDOFRevised(glm::vec2 resolution, float time, int x, int y, glm::vec3 eye, glm::vec3 view, glm::vec3 up, glm::vec3 right, float fovy, float aperture, float focalDistance);


__global__ void createRayPool( camera cam, rayPoolElement *rpe, float iteration) {
  int y = blockDim.y * blockIdx.y + threadIdx.y;
  int x = blockDim.x * blockIdx.x + threadIdx.x;
  
  if(y < cam.res.y && x < cam.res.x) {
    int currentIndex = y * cam.res.x + x;

    //for Depth of field effect
    //aperture is Radius of lens "disc"
    ray viewRay;
    if(cam.isDOFEnable) {
      viewRay = rayWithDOFRevised(cam.res, iteration, x, y, cam.position, cam.view, cam.up, cam.right, cam.fovy, cam.aperture, cam.focalDistance);
    } else {
      viewRay = raycastFromCameraKernel(cam.res, iteration, x, y, cam.position, cam.view, cam.up, cam.right, cam.fovy);
    }
    //ray viewRay = rayWithDOF(cam.res, iteration, x, y, cam.position, cam.view, cam.up, cam.right, cam.fovy);

    rpe[currentIndex].isValid = true;
    rpe[currentIndex].r = viewRay;
    rpe[currentIndex].index = currentIndex;
  }
}

__host__ __device__ ray raycastFromCameraKernel(glm::vec2 resolution, float time, int x, int y, glm::vec3 eye, glm::vec3 view, glm::vec3 up, glm::vec3 right, float fovy){
  //darshan 
  //#ifdef __CUDA_ARCH__
  //	std::cout<<"run on cuda\n";
  //#else
  //	std::cout<<"run on cpu\n";
  //#endif

  //hare krishna
  //SUMMERY:- For ray we need 2 points one is camera position and other is calculated from pixel
  ray r;
  //I. convert pixel format to normalize form in range X=[-1,1], Y=[-1,1]
  //formulae- sx = 2*px/x - 1, sy = 1 - 2*py/y
  thrust::default_random_engine rng(hash(time*x*resolution.x+y));
  thrust::uniform_real_distribution<float> u0p5(-0.5, 0.5);
  float sx = 2.0f*(x + u0p5(rng))/(float)resolution.x - 1; //THis all calculated based on texture mapping
  float sy = 2.0f*(y + u0p5(rng))/(float)resolution.y - 1;

  //II. Calculate Horizontal(H) and Vertical(V) vectors on plane perpendicular to view vector and at 5 unit away of camera (0,4.5,7)
  //V = up * magnitude, magnitude = distance * tan(fovy/2)
  glm::vec3 ref = eye+view;//glm::vec3(0,4.5,7);
  float len = 1;//glm::length(ref-eye);
  float mag = /*len **/ tan(fovy * PI /(2.0f * 180.0f) );
  glm::vec3 V = up * mag; 
  //H = cross(view,up) * magnitude * aspect
  
  glm::vec3 H =  right* mag * (resolution.x)/(resolution.y) ;
  //glm::vec3 H = glm::cross(glm::vec3(0,0,-1),glm::vec3(0,1,0)) * mag * (float)(resolution.x)/(float)(resolution.y) ;

  //III. Calculating acutual point correspoinding to pixel position
  // multiply normalized X (sx) with H vector and normalized Y (sy) wiht V vector
  //point P = Ref + H*sx + V*sy
  glm::vec3 P = ref + H*sx + V*sy;

  //IV. Assign ray origin and ray direction.
  //direction = P - eye
  r.origin = eye;
  r.direction = glm::normalize(P - eye);

  return r;
}



__host__ __device__ ray rayWithDOFRevised(glm::vec2 resolution, float time, int x, int y, glm::vec3 eye, glm::vec3 view, glm::vec3 up, glm::vec3 right, float fovy, float aperture, float focalDistance) {
  thrust::default_random_engine rng(hash(time)*hash(x*resolution.x+y+1));
  thrust::uniform_real_distribution<float> u0t1(0, 1);

  //STEP1:: getting primary ray
  ray primaryR = raycastFromCameraKernel(resolution, time, x, y, eye, view, up, right, fovy);

  //STEP2:: getting secondary ray, randomly choosen from aperture radius
  ///get focal point on primary ray
  glm::vec3 focalPoint = eye + focalDistance * primaryR.direction;

  ///calculate random point around eye in image plane with radius of radius lens or Aperature
  //we need random vector in plane with vector up and right
  //'theta' a random value lies in [0,2*PI]
  float theta = 2*PI*u0t1(rng);
  //so the random value in plane with given pair of perpendicular vector calculated with same phenomenea as in X-Y plane
  float randomRadius = aperture * u0t1(rng);
  glm::vec3 randomLensePoint = randomRadius * (right*cos(theta) + up*sin(theta));
  //getting actual point from vector
  randomLensePoint = eye+randomLensePoint;
  
  ray secondaryR(randomLensePoint, glm::normalize(focalPoint-randomLensePoint));

  return secondaryR;
}



//Yups its correct one, but we are implimenting more efficient and adustable function above on 29-Aug-2017
//Ray with DOF need to deal with random generating thing
__host__ __device__ ray rayWithDOF(glm::vec2 resolution, float time, int x, int y, glm::vec3 eye, glm::vec3 view, glm::vec3 up, glm::vec3 right, float fovy){
  ray r;
  
  //I. convert pixel format to normalize form in range X=[-1,1], Y=[1,-1]
  //formulae- sx = 2*px/x - 1, sy = 1 - 2*py/y
  float sx = 2.0f*(x)/(float)resolution.x - 1;
  float sy = 2.0f*(y)/(float)resolution.y - 1;
  
  //II. Calculate Horizontal(H) and Vertical(V) vectors on plane perpendicular to view vector and at 5 unit away of camera (0,4.5,7)
  //V = up * magnitude, magnitude = distance * tan(fovy/2)
  glm::vec3 ref = eye+view;
  float len = 1;
  float mag = /*1 **/ tan(fovy * PI /(2.0f * 180.0f) );
  glm::vec3 V = up * mag;
  //H = cross(view,up) * magnitude * aspect
  glm::vec3 H = glm::cross(view,up) * mag * (float)(resolution.x)/(float)(resolution.y) ;

  //III. Calculating acutual point correspoinding to pixel position
  // multiply normalized X (sx) with H vector and normalized Y (sy) wiht V vector
  //point P = Ref + H*sx + V*sy
  glm::vec3 P = ref + H*sx + V*sy;

  //IV. get ray direction
  glm::vec3 direc = glm::normalize(P-eye);

  //V. calculate focal point which is at 14 unit away focusing on object 7
  float focusDistance = 10;//14;
  glm::vec3 FocalPoint = eye + direc*focusDistance;

  //VI. get random pixel from group of 8 nearby pixels including itself & also cover here corner cases
  thrust::default_random_engine rng(hash(time)*hash(x*resolution.x+y));
  thrust::uniform_real_distribution<float> u0p9(0, 9);


  int randomPixel = u0p9(rng);//getRandomNumber(45*time+x+y, 0, 9);

  //So we are not considering corener cases
  if(randomPixel < 1 || randomPixel > 8 || x==0 || y==0 || (x==resolution.x-1) || (y==resolution.y-1)) {
    //IV. Assign ray origin and ray direction.
    //direction = P - eye
    r.origin = eye;
    r.direction = glm::normalize(P - eye);
  }

  thrust::uniform_real_distribution<float> uM20p20(-20, 20);
  r = raycastFromCameraKernel(resolution, time, x+(int)uM20p20(rng), y+(int)uM20p20(rng), eye, view, up, right, fovy);

  //switch(randomPixel) {
  //case 1: {
  //  r = raycastFromCameraKernel(resolution, time, x-10, y-10, eye, view, up, fov);
  //  break;
  //}
  //case 2: {
  //  r = raycastFromCameraKernel(resolution, time, x, y-10, eye, view, up, fov);
  //  break;
  //}
  //case 3: {
  //  r = raycastFromCameraKernel(resolution, time, x+10, y-10, eye, view, up, fov);
  //  break;
  //}
  //case 4: {
  //  r = raycastFromCameraKernel(resolution, time, x-10, y, eye, view, up, fov);
  //  break;
  //}
  //case 5: {
  //  r = raycastFromCameraKernel(resolution, time, x+10, y, eye, view, up, fov);
  //  break;
  //}
  //case 6: {
  //  r = raycastFromCameraKernel(resolution, time, x-10, y+10, eye, view, up, fov);
  //  break;
  //}
  //case 7: {
  //  r = raycastFromCameraKernel(resolution, time, x, y+10, eye, view, up, fov);
  //  break;
  //}
  //case 8: {
  //  r = raycastFromCameraKernel(resolution, time, x+10, y+10, eye, view, up, fov);
  //  break;
  //}
  //default:
  //  break;
  //}

  glm::vec3 imagePoint;
  float imagePlaneDistance = 5;

  imagePoint = eye + r.direction * imagePlaneDistance;

  r.direction = glm::normalize(FocalPoint - imagePoint);
  r.origin = imagePoint;

  return r;

}



#endif //CAMERA_HANDLER_H