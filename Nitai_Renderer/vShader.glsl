//Hare Krishna Hare Krishna
//Krishna Krishna Hare Hare


in vec2 vsPosition;
in vec2 vsUV;

out vec2 fsUV;

void main() {
  gl_Position = vec4(vsPosition.xy, 0, 1);
  fsUV = vsUV;
}