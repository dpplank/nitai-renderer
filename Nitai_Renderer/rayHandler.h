#ifndef RAY_HANDLER_H
#define RAY_HANDLER_H

#include "structsWorld.h"
#include "rayObject.h"
#include "cudaUtils.h"
#include "rayMaterial.h" //for epsiloneCheck function

__device__ bool giveIntersection(ray viewRay, object* geoms, int numberOfGeoms, GPUMesh *mesh, int numberOfMeshes, glm::vec3 *minPoint, glm::vec3 *minNormal, object *finalGeom){
  //for each objects
  float minT = 10000;
  for(int i=0; i<numberOfGeoms; i++) {
    float inT;
    glm::vec3 inPoint;
    glm::vec3 inNormal;
    object currentGeom= geoms[i]; ///PERFORMANCE OPTIMIZATION::: NEED TO REMOVE THIS COPY
    //test for current object is at minimum distance
    switch(geoms[i].type) {
      case SPEHRE:
        inT = raySphereTest(currentGeom, viewRay, inPoint, inNormal);
        break;
      case CUBE:
        inT = rayBoxTest(currentGeom, viewRay, inPoint, inNormal); 
        break;
      default:
          //none
    }

    if( inT > 0 && inT < minT ) {    
      minT=inT;
      *minPoint = inPoint;
      *minNormal = glm::normalize(inNormal);
      *finalGeom = currentGeom;
    }
  }

  //Testing with mesh
  for(int i=0; i<numberOfMeshes; i++) {
    float inT;
    glm::vec3 inNormal;
    glm::vec3 inPoint;
    inT = rayMeshTest(mesh, viewRay, inPoint, inNormal);
    
    if(inT > 0 && inT < minT) {
      minT = inT;
      *minPoint = inPoint;
      *minNormal=glm::normalize(inNormal);
      *finalGeom = *(object*)mesh; //Need to check this//Checked it's done good
    }
  }

  if(epsilonCheck(minT,10000))
    return false;
  
  return true;
}



#endif //RAY_HANDLER_H