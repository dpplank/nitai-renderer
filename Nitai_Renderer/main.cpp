#include <iostream>
#include <stdlib.h>
#include <rapidjson\document.h>

#define GLM_SWIZZLE
#include "utils.h"
#include "scene.h"
#include "cudaOpengl.h"
#include "displayFuncs.h"
#include <GL/freeglut.h>
#include "pathTracer.h"
#include "structsWorld.h"
#include <glm/gtc/matrix_transform.hpp>

#include <cuda_gl_interop.h>

#define DEBUG 1

using namespace std;

////GLUT CLLABLE FUNCTIONS///
void mouseClicking(GLint button, GLint state, GLint x, GLint y);
void mouseDragging(GLint x, GLint y);
void specialKeyPressControl(int key, int x, int y);
void specialKeyReleaseControl(int key, int x, int y);
void keyPressControl(unsigned char key, int x,  int y);

///OTHER FUNCTIONS///
void checkAndSaveImage();

//global variable
cudaOpengl cudaGL;

//temp camera, need to do it with scene loading
camera c;
scene world;


int main(int argc, char **argv) {
  std::cout<<"Hare Krishan"<<endl;

  cout<<"size of unsigned int: "<<sizeof(unsigned int)<<endl;
  cout<<"size of int: "<<sizeof(int)<<endl;

  world.createSceneFromFile("abc");

  cudaGL.init(argc, argv, world.cam.res.x, world.cam.res.y);
  OPENGL_ERROR
  cudaGL.setDisplayFunc(render);
  cudaGL.setMouseFunc(mouseClicking);
  cudaGL.setMotionFunc(mouseDragging);
  cudaGL.setSpecialKeyFunc(specialKeyPressControl);
  cudaGL.setSpecialKeyUpFunc(specialKeyReleaseControl);
  cudaGL.setKeyboardFunc(keyPressControl);

  cudaGL.startLoop();

  system("Pause");
  return 0;
}

int oldTime = 0;

 int getFPS() {
    int newTime = glutGet(GLUT_ELAPSED_TIME);
    int secondsPerFram = (newTime - oldTime) / 1000.0f;
   /* if(secondsPerFram>1) {
      return secondsPerFram;
    }*/
    int fps = 1000.0/(float)(newTime - oldTime);
    oldTime = newTime;
    return fps;
 }

 //int frameCountGlobal=0;
 int frameCount=0;

void render() {
  //frameCountGlobal++;
  frameCount++;

  int fps = getFPS();
  //change title tile with show of fps
  char title[50];
  sprintf(title, "NITAI RENDERER || FPS : %d || Count: %d", fps, frameCount);
  glutSetWindowTitle(title);

  OPENGL_ERROR
  glClear(GL_COLOR_BUFFER_BIT);
  //cout<<"inside render"<<endl;
  //call cuda function to run and fill the PBO of openGL
  launchCUDA();
  
  //bind pbo 
  cudaGL.bindPBO();
  OPENGL_ERROR
  //bind texture 
  cudaGL.bindTexture();
  OPENGL_ERROR
  //finally draw our 2 triangles
  cudaGL.bindIndex();

  OPENGL_ERROR
  cudaGL.draw();
  OPENGL_ERROR

  cudaGL.swapBuffer();

  ///Checking iteration limit and saving image
  checkAndSaveImage();

  glutPostRedisplay();
}

void launchCUDA() {
  //attach cudaGL's pbo to CUDA to write
  //NOTE: The resources in resources may be accessed by CUDA until they are unmapped. 
  //      The graphics API from which resources were registered should not access any 
  //      resources while they are mapped by CUDA. If an application does so, the results are undefined.
  CUDA_ERROR(cudaGraphicsMapResources(1, &(cudaGL.pboCUDAResource), 0));

  //get Pointer to write from CUDA kernel
  uchar4 *cudaPBO;
  size_t sizeInBytes;
  CUDA_ERROR(cudaGraphicsResourceGetMappedPointer((void**)&cudaPBO, &sizeInBytes, cudaGL.pboCUDAResource));
  //cudaGLMapBufferObject((void**)&cudaPBO, cudaGL.pbo);

  //launch our rendering functoin to fill pbo
  renderFrame(cudaPBO, world, frameCount);

  CUDA_LAST_ERROR

  //detach cudaGL's pbo from CUDA
  CUDA_ERROR(cudaGraphicsUnmapResources(1, &cudaGL.pboCUDAResource, 0));
  //cudaGLUnmapBufferObject(cudaGL.pbo);
}


void checkAndSaveImage() {
  if(world.cam.totalSamples <= frameCount) {
    char *fileName = "../images/hari_hari_g1.bmp";

    //Save image stored into world.cam
    utils::createImage(world.cam.image, world.cam.res.x, world.cam.res.y, fileName);

    //make frameCount = 0;
    frameCount=0;

    if(DEBUG) {
      int debugVal=9;
      debugVal = 2;
    }

    exit(0);
  }
}




bool isDragging=false;
bool isZooming=false;
int oldX, oldY;


void mouseClicking(GLint button, GLint state, GLint x, GLint y) {
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		cout<<"into left pressed state"<<endl;
		oldX = x;
		oldY = y;
		isDragging = true;
	}

	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		cout<<"into left released state"<<endl;
		isDragging = false;
	}

	if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		oldX = x;
		oldY = y;
		isZooming = true;
		oldTime = glutGet(GLUT_ELAPSED_TIME);
	}

	if(button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		isZooming = false;
	}
}

void updateCameraSpecs();

void mouseDragging(GLint x, GLint y) {
	if(isDragging) {
		int dX, dY;
		dX = x - oldX;
		dY = y - oldY;
    world.cam.theta -= dX;
		world.cam.phi += dY;
    updateCameraSpecs();
    //Refreshing the image
    frameCount=0;

		oldX = x;
		oldY = y;
		//cout<<"OldX: "<<oldX;	//
		//cout<<"OldY: "<<oldY;	//
	}
	if(isZooming) {
		int dX, dY;
		dX = x - oldX;
		dY = y - oldY;

		//calculating dT
		int dT, t;
		t = glutGet(GLUT_ELAPSED_TIME);
		dT = t-oldTime;

    if(dT < 10) {
      dT = 10;
    }

		//Calculation speed
		float speedX = (dX>0 ? dX : -dX)/(float)dT;
		float speedY = (dY>0 ? dY : -dY)/(float)dT;
		//Limiting speed between [0.1, 1]
		speedX = (speedX<0.1 ? 0.1 : (speedX>1 ? 1 : speedX));
		speedY = (speedY<0.1 ? 0.1 : (speedY>1 ? 1 : speedY));

    world.cam.zoom += -1 * dX * 2 * speedX;
		world.cam.zoom +=  dY * 2 * speedY;

    updateCameraSpecs();
    //refreshing the image
    frameCount=0;

		oldX = x;
		oldY = y;

	}
	glutPostRedisplay();
}

bool isCtrlPressed = false;
float stepSize = 0.2;

void specialKeyPressControl(int key, int x, int y) {
  if(key == GLUT_KEY_RIGHT) {
    //c.ref.x += 1.0;
    glm::vec3 toMove = utils::getRotation(glm::vec3(1,0,0), glm::vec3(0,1,0), c.theta);
    world.cam.ref += stepSize*toMove;
    updateCameraSpecs();
    frameCount=0;
    glutPostRedisplay();
  }

  if(key== GLUT_KEY_LEFT) {
    glm::vec3 toMove = utils::getRotation(glm::vec3(1,0,0), glm::vec3(0,1,0), c.theta);
    world.cam.ref -= stepSize*toMove;
    updateCameraSpecs();
    frameCount=0;
    glutPostRedisplay();
  }

  if(key == GLUT_KEY_UP) {

    if(!isCtrlPressed) {
      //move c.ref in X-Z plane,forwardly along direction camera is viewing
      glm::vec3 toMove = utils::getRotation(glm::vec3(0,0,-1), glm::vec3(0,1,0), c.theta);
      //c.ref.z -= 1.0;
      world.cam.ref += stepSize*toMove;
      updateCameraSpecs();
      frameCount=0;
      glutPostRedisplay();
    } else {
      //For upside and downside
      glm::vec3 toMove(0,1,0);
      world.cam.ref += stepSize*toMove;
      updateCameraSpecs();
      frameCount=0;
      glutPostRedisplay();
    }
  }
    
  if(key == GLUT_KEY_DOWN) {
    if(!isCtrlPressed) {
      //move c.ref in X-Z plane, backwardly along direction camera is viewing
      glm::vec3 toMove = utils::getRotation(glm::vec3(0,0,-1), glm::vec3(0,1,0), c.theta);
      world.cam.ref -= stepSize*toMove;
      updateCameraSpecs();
      frameCount=0;
      glutPostRedisplay();
    } else {
      //For upside and downside
      glm::vec3 toMove(0,1,0);
      world.cam.ref -= stepSize*(toMove);
      //world.cam.ref.y = std::max(c.ref.y, 0.0f);
      updateCameraSpecs();
      frameCount=0;
      glutPostRedisplay();
    }
  }

  if(key == GLUT_KEY_CTRL_L || key == GLUT_KEY_CTRL_R) {
    isCtrlPressed=true;
  }

}

void specialKeyReleaseControl(int key, int x, int y) {
  if(key == GLUT_KEY_CTRL_L || key == GLUT_KEY_CTRL_R) {
    isCtrlPressed=false;
  }
}

void keyPressControl(unsigned char key, int x,  int y) {
  //ctrl preseed will change aperture of camera
  /*if(isCtrlPressed) */{
    if(key=='k') {
      world.cam.aperture += 0.1;
      std::cout<<"increase Aperture"<<std::endl;
      frameCount=0;
      return;
    }
    if(key=='l') {
      world.cam.aperture -= 0.1;
      std::cout<<"decrese Aperture"<<std::endl;
      frameCount=0;
      return;
    }
  } /*else*/ {
    if(key=='i') {
      world.cam.focalDistance += 1;
      frameCount=0;
      return;
    }
    if(key=='o') {
      world.cam.focalDistance -= 1;
      frameCount=0;
      return;
    }    

  }
}


void updateCameraSpecs() {
  float phiRad = world.cam.phi*PI/180;
  float thetaRad = world.cam.theta*PI/180;
  glm::vec3 posVec (world.cam.zoom * cos(phiRad) * sin(thetaRad) ,
                world.cam.zoom * sin(phiRad), //Move up as "phi" increase
                world.cam.zoom * cos(phiRad) * cos(thetaRad));

  world.cam.position = world.cam.ref + posVec;
  world.cam.view = -glm::normalize(posVec);

  //calculate axis about which to rotate up vector
  glm::vec4 secAxis = glm::rotate(glm::mat4(1),world.cam.theta,glm::vec3(0,1,0))*glm::vec4(-1,0,0,0);
	world.cam.up = (glm::rotate(glm::mat4(1), world.cam.phi, glm::vec3(secAxis.xyz)) * glm::vec4(0,1,0,0)).xyz; //BECAUSE OF GLM_SWIZZLE from documentation

  world.cam.right = glm::cross(world.cam.view, world.cam.up);
}