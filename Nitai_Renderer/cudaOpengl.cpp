#include "cudaOpengl.h"
#include <GL/freeglut.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h> //for cuda and OpenGL Inter operation


#include "utils.h"

cudaOpengl::cudaOpengl(void) {
  width = 500;
  height = 500;
}


cudaOpengl::~cudaOpengl(void) {
  cudaGraphicsUnregisterResource(pboCUDAResource);
}

void cudaOpengl::init(int argc, char **argv, int inWidth, int inHeight) {
  width = inWidth;
  height = inHeight;
  initGlut(argc, argv);
  initGL();
  initPboCuda();
  OPENGL_ERROR
}

void cudaOpengl::initGlut(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(0,0);
  glutCreateWindow("NITAI RENDERER");  
}

void cudaOpengl::initGL() {
  glClearColor(1, 1, 1, 1);
  GLenum err = glewInit();

  if(err != GLEW_OK) {
    std::cerr<<"PROBLEM OF INITIALIZING GLEW"<<std::endl;
    system("pause");
    exit(0);
  }

	GLuint vao;
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);


  //create polygon coordinates to cover screen
  glm::vec2 coord[4] = {
    glm::vec2(-1, -1),
    glm::vec2( 1, -1),
    glm::vec2( 1,  1),
    glm::vec2(-1,  1)
  };

  //create UVs for polygon
  glm::vec2 UVs[4] = {
    glm::vec2(0, 0),
    glm::vec2(1, 0),
    glm::vec2(1, 1),
    glm::vec2(0, 1),
  };

  //create Indicies to represent 2 triangles
  GLuint index[6] = {
    0, 1, 2,
    2, 3, 0
  };

  //send all above values in GPU
  glGenBuffers(1, &bufPos);
  glBindBuffer(GL_ARRAY_BUFFER, bufPos);
  glBufferData(GL_ARRAY_BUFFER, 4*sizeof(glm::vec2), coord, GL_STATIC_DRAW);

  glGenBuffers(1, &bufUV);
  glBindBuffer(GL_ARRAY_BUFFER, bufUV);
  glBufferData(GL_ARRAY_BUFFER, 4*sizeof(glm::vec2), UVs, GL_STATIC_DRAW);

  glGenBuffers(1, &bufIdx);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(GLuint), index, GL_STATIC_DRAW);

  //Create shader program
  createShader("vShader.glsl", "fShader.glsl");

  //attach shader variables with Position and UVs
  glBindBuffer(GL_ARRAY_BUFFER, bufPos);
  glEnableVertexAttribArray(vPos);
  glVertexAttribPointer(vPos, 2, GL_FLOAT, GL_FALSE, 0, NULL);

  glBindBuffer(GL_ARRAY_BUFFER, bufUV);
  glEnableVertexAttribArray(vUV);
  glVertexAttribPointer(vUV, 2, GL_FLOAT, GL_FALSE, 0, NULL);
}

void cudaOpengl::initPboCuda() {
  ///create buffer memory for pbo of size widht*height*RGBA
  ///Also make it with GL_PIXEL_UNPACK_BUFFER // for pixel data being passed into OpenGL
  int pixelBufferSize = width*height*4/*RGBA*/*sizeof(GLubyte);
  glGenBuffers(1, &pbo);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
  glBufferData(GL_PIXEL_UNPACK_BUFFER, pixelBufferSize, NULL, GL_DYNAMIC_DRAW);

  ///Make a texture of size widht*height, and point it to null, because by doing that,
  ///because then GL_PIXEL_UNPACK_BUFFER will be used as source of data for command glTexImage2D
  //bind GL_PIXEL_UNPACK_BUFFER to 0 so that makin texture not in result any copying

  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  //At this point our texture as undefined value

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);//gl_linear, take average of near pixel to avoid anialiasing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  ///CUDA-OpenGL registering
  //set cuda device to 0, for only GPU
  cudaGLSetGLDevice(0);
  //register pbo to cuda and map openGL pbo to pboCUDA
  
  CUDA_ERROR(cudaGraphicsGLRegisterBuffer(&pboCUDAResource, pbo, cudaGraphicsRegisterFlagsNone));
}

void cudaOpengl::createShader(const char *vShader, const char *fShader) {
  GLuint program;

  struct shaderData {
	GLenum type;
	const char* name;
	char * source;
  };

	//initialize shader data
	shaderData sData[2] = 
	{{GL_VERTEX_SHADER, vShader, NULL},
	 {GL_FRAGMENT_SHADER, fShader, NULL}};
	//initialize shader program to empty program
	program = glCreateProgram();
	
	//loop for every shader
	for(int i=0; i<2; i++) {		
		//assign source
		sData[i].source = utils::getFileToChar(sData[i].name);

		//allocate shader object
		GLuint shaderObj = glCreateShader(sData[i].type);
		//associate sorce of shader to just created shader object
		const GLchar *source[] = {sData[i].source};
		glShaderSource(shaderObj, 1, source, NULL);	
		//free up memory of source file
		delete []sData[i].source;
		//compile this shader object
		glCompileShader(shaderObj);
		
		//check for any error
		GLint status;
		glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &status);
		if(!status) {	//if status is not good
			//get information about error
			GLint size;
			glGetShaderiv(shaderObj, GL_INFO_LOG_LENGTH, &size);
			char *infoLog = new char[size];
			glGetShaderInfoLog(shaderObj, size, NULL, infoLog);
      if(i==0) std::cout<<"Error in Vertex Shader: ";
      else     std::cout<<"Error in Fragment Shader: ";
      std::cout<<"Error occured while compiling shader: "<<infoLog<<std::endl;
      delete []infoLog;
      system("pause");
		}

		//attach this shader object to our core shader program
		glAttachShader(program, shaderObj);
	}
	//link the program
	glLinkProgram(program);
	//use the program as being current, just for default behavior
	glUseProgram(program);

	//Initialize all shader variable location
	vPos = glGetAttribLocation(program, "vsPosition");
  vUV = glGetAttribLocation(program, "vsUV");

	std::cout<<"vPos: "<<vPos<<std::endl;
  std::cout<<"vsUV: "<<vUV<<std::endl<<std::endl;

  printf("debug");

}

void cudaOpengl::setDisplayFunc(void (*func)(void)) {
  glutDisplayFunc(func);
}

void cudaOpengl::setMouseFunc(void(*func)(GLint button, GLint state, GLint x, GLint y)) {
  glutMouseFunc(func);
}

void cudaOpengl::setMotionFunc(void(*func)(GLint x, GLint y)) {
  glutMotionFunc(func);
}

void cudaOpengl::setSpecialKeyFunc(void(*func)(int key, int x, int y)) {
  glutSpecialFunc(func);
}

void cudaOpengl::setSpecialKeyUpFunc(void(*func)(int key, int x, int y)) {
  glutSpecialUpFunc(func);
}

void cudaOpengl::setKeyboardFunc(void(*func)(unsigned char key, int x, int y)) {
  glutKeyboardFunc(func);
}

void cudaOpengl::startLoop() {
  glutMainLoop();
}

void cudaOpengl::bindPBO() {
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
}

void cudaOpengl::bindTexture() {
  //call glTexSubImage2D with NULL data, as it will copy from memory with GL_PIXEL_UNPACK_BUFFER
  //which is "pbo"
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
}

void cudaOpengl::bindIndex() {
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
}

void cudaOpengl::draw() {
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void cudaOpengl::swapBuffer() {
  glutSwapBuffers();
}
