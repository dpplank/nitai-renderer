#ifndef RAY_MATERIAL_H
#define RAY_MATERIAL_H

#include "structsWorld.h"
#include <thrust\random.h>
#include "cudaUtils.h"

#define EPSILON .0000001
#define SQRT_OF_ONE_THIRD           0.5773502691896257645091487805019574556476

enum InteractionType {SCATTER, REFLECT, REFRECT};

__host__ __device__ int hash(int k) { //k=key
  //Robert Jenkins type function
  k = ~k+(k << 15);
  k = k^(k>>12);
  k = k+(k<<2);
  k = k^(k>>4);
  k = k*2145; //or 2057
  k = k^(k>>16);
  
  return k;
}

__host__ __device__ bool epsilonCheck(float a, float b) {
  if(fabs(fabs(a)-fabs(b)) < EPSILON) {
    return true;
  }
  return false;
}


__host__ __device__ glm::vec3 getRandomDirHemiSphere(glm::vec3 normal, float xi1, float xi2) {
    //crucial difference between this and calculateRandomDirectionInSphere: THIS IS COSINE WEIGHTED!
    float up = sqrt(xi1); // cos(theta)
    float over = sqrt(1 - up * up); // sin(theta)
    float around = xi2 * 2*PI;
    
    //Find a direction that is not the normal based off of whether or not the normal's components are all equal to sqrt(1/3) or whether or not at least one component is less than sqrt(1/3). Learned this trick from Peter Kutz.
    glm::vec3 directionNotNormal;
    if (abs(normal.x) < SQRT_OF_ONE_THIRD) { 
      directionNotNormal = glm::vec3(1, 0, 0);
    } else if (abs(normal.y) < SQRT_OF_ONE_THIRD) { 
      directionNotNormal = glm::vec3(0, 1, 0);
    } else {
      directionNotNormal = glm::vec3(0, 0, 1);
    }    
    //Use not-normal direction to generate two perpendicular directions
    glm::vec3 perpendicularDirection1 = glm::normalize(glm::cross(normal, directionNotNormal));
    glm::vec3 perpendicularDirection2 = glm::normalize(glm::cross(normal, perpendicularDirection1)); 
    
    //D: This is wrong, becuse it is doing things like Camera transformation, but here we need inverse transformation
    //D: Later I found, my just above statement is wrong, karl doing same thing
    //return ( up * normal ) + ( cos(around) * over * perpendicularDirection1 ) + ( sin(around) * over * perpendicularDirection2 );
    
    glm::vec3 normalVec (over*cos(around), up, over*sin(around));
    glm::vec3 result( perpendicularDirection1.x * normalVec.x + normal.x * normalVec.y + perpendicularDirection2.x*normalVec.z,
                      perpendicularDirection1.y * normalVec.x + normal.y * normalVec.y + perpendicularDirection2.y*normalVec.z,
                      perpendicularDirection1.z * normalVec.x + normal.z * normalVec.y + perpendicularDirection2.z*normalVec.z);
    return result;

}

/////NOTE::: SOME ERROR in MINE VERSION LOOK LATER
__host__ __device__ glm::vec3 getRandomDirHemiSphere1(glm::vec3 normal, float val1, float val2) {
  ///Random vector in hemisphere about Y-Axis in standard coordinate system
  //theta: angle from Y-Z plane | phi: angle from X-Z plane
  //val1 = cos(phi)
  //val2 = [-1,1] | theta = 2*PI*val2
  //Random vector in statandard coordinate system:: x=cos(phi)*sin(theta) | y=sin(phi) | z = cos(phi)cos(theta)
  float theta = 2*PI*val2;
  float x = val1*sin(theta);
  float y = sqrt(1-val1*val1);
  float z = val1 * cos(theta);

  ///Defining Coordinate system 
  //if normal is not Z-Axis
  glm::vec3 coordX;
  if(!epsilonCheck(normal.z, 1)) {
    coordX = glm::normalize(glm::vec3(normal.y, -normal.x, 0));
  } else {
    coordX = glm::normalize(glm::vec3(1, 0, 0));
  }
  glm::vec3 coordZ = glm::cross(coordX, normal);

  //Using change of Basis, changing x,y,z to Basis around normal.
  //LEARNING: to varify change of basis matrix multiply it with x-axis([1,0,0]), y-axis([0,1,0]) and z-axis([0,0,1])
  return x*coordX + y*normal + z*coordZ;
}

__host__ __device__ glm::vec3 getReflectedDirection(glm::vec3 normal, glm::vec3 inDirection) {
	//reflected ray Rr = incident - 2*normal*dot(incident,normal);
	glm::vec3 Rr = inDirection - 2.0f * normal * glm::dot(inDirection,normal);

	return glm::normalize(Rr);
}

__host__ __device__ glm::vec3 getGlossyDirection(glm::vec3 reflectedDir, float val1, float val2, float glossyNess) {
  //crucial difference between this and calculateRandomDirectionInSphere: THIS IS COSINE WEIGHTED!
  float up = sqrt(val1); // cos(theta)
  float over = sqrt(1 - up * up); // sin(theta)
  float around = val2 * 2*PI;
    
  //Find a direction that is not the normal based off of whether or not the normal's components are all equal to sqrt(1/3) or whether or not at least one component is less than sqrt(1/3). Learned this trick from Peter Kutz.
  glm::vec3 directionNotNormal;
  if (abs(reflectedDir.x) < SQRT_OF_ONE_THIRD) { 
    directionNotNormal = glm::vec3(1, 0, 0);
  } else if (abs(reflectedDir.y) < SQRT_OF_ONE_THIRD) { 
    directionNotNormal = glm::vec3(0, 1, 0);
  } else {
    directionNotNormal = glm::vec3(0, 0, 1);
  }    
  //Use not-normal direction to generate two perpendicular directions
  glm::vec3 perpendicularDirection1 = glm::normalize(glm::cross(reflectedDir, directionNotNormal));
  glm::vec3 perpendicularDirection2 = glm::normalize(glm::cross(reflectedDir, perpendicularDirection1)); 
    
  //D: This is wrong, becuse it is doing things like Camera transformation, but here we need inverse transformation
    //D: Later I found, my just above statement is wrong, karl doing same thing
    //return ( up * normal ) + ( cos(around) * over * perpendicularDirection1 ) + ( sin(around) * over * perpendicularDirection2 );
    
  glm::vec3 normalVec = glm::normalize( glm::vec3((over*cos(around)/glossyNess), up*glossyNess, (over*sin(around)/glossyNess)) );
  glm::vec3 result( perpendicularDirection1.x * normalVec.x + reflectedDir.x * normalVec.y + perpendicularDirection2.x*normalVec.z,
                    perpendicularDirection1.y * normalVec.x + reflectedDir.y * normalVec.y + perpendicularDirection2.y*normalVec.z,
                    perpendicularDirection1.z * normalVec.x + reflectedDir.z * normalVec.y + perpendicularDirection2.z*normalVec.z);
  return result;

  glm::vec3 newDir = getRandomDirHemiSphere(reflectedDir, val1, val2);
  newDir.x = newDir.x/ glossyNess;
  newDir.z = newDir.z/ glossyNess;
  return newDir;
}


//Darshan:: My version of BSDF, currently support only diffuse, reflection and refraction
// It calculate new ray direction based on material property
// Extra: we are usng const and reference to save space
__host__ __device__ glm::vec3 getMyBSDF (const ray r, const glm::vec3 &hitPoint, const glm::vec3 &hitNormal, const material m, InteractionType &rayStatus, float iteration, int bounces, int row) {
  bool isRefractive = m.isRefrect;
  bool isReflective = m.isReflect;
  float indexOfRefrec = m.indexOfRefraction;
  glm::vec3 newDirection;
  
  thrust::default_random_engine rng(hash(iteration * (bounces+1))*hash(row));
  thrust::uniform_real_distribution<float> u0to1(0, 1);

  //1. PURE DIFFUSE SURFACE
  if(!isRefractive && !isReflective && epsilonCheck(indexOfRefrec, 0) ) {
    //It is diffuse surface
    rayStatus=InteractionType::SCATTER;
    newDirection = getRandomDirHemiSphere(hitNormal, u0to1(rng), u0to1(rng)); /*calculateReflectionDirection(normal, rpe[row].r.direction);*/
    return newDirection;
  }

  //2. FRESNEL REFLECTION SURFACE
  if(!isReflective && !isRefractive ) {
    //Using sleek's approximation to determine reflection
    float absCosTheta = abs(glm::dot(r.direction, hitNormal));//abs will handle both situations for entering into or leaving from object for sleeks approximation
    
    float Ro = pow( (1 - indexOfRefrec) / (1 + indexOfRefrec), 2);

    float ProbablityOfReflection = Ro + (1-Ro)*pow((1 - absCosTheta), 5);

    //First for reflection case
    if(u0to1(rng) < ProbablityOfReflection) {
      //Making isReflective true and treating it as a reflective surface
      isReflective = 1;
    } else {
      rayStatus=InteractionType::SCATTER;
      newDirection = getRandomDirHemiSphere(hitNormal, u0to1(rng), u0to1(rng)); /*calculateReflectionDirection(normal, rpe[row].r.direction);*/
      return newDirection;
    }
  }

  if(isRefractive) {
    //REMEMBER(for n1 and n2)::= light is entering for n1 medium to n2 medium
    //It is refractive surface, treate as glass, using sleek's approximation to determine = reflection or refraction
    //First determine ray is entering object or leaving object.
    //R(theta) = Ro + (1-Ro)*pow((1-cos(theta)), 5), Ro is value of R(theta) at 0 degree
    // Ro = pow((n1 - n2)/(n1 + n2), 2)

    float cosTheta = glm::dot(r.direction, hitNormal); 
    //absCosTheta will give use cos(theta) value between Normal pointed towards the face from where light is coming
    float absCosTheta = abs(cosTheta);//abs will handle both situations for entering into or leaving from object for sleeks approximation
    float indexOfRefl = m.indexOfRefraction;

    float Ro = pow( (1 - indexOfRefl) / (1 + indexOfRefl), 2);

    float ProbablityOfReflection;
    
    //if n2 > n1, we use as usual formulae R(theta) = Ro + (1-Ro)*pow((1-cos(theta)), 5)
    if(cosTheta < 0) { //ray entering surface 
      ProbablityOfReflection = Ro + (1-Ro)*pow((1 - absCosTheta), 5);
    } else { //we use as usual formulae R(theta) = Ro + (1-Ro)*pow((1-cos(thetaT)), 5), thetaT is transmitted angle
      float sinThetaI = sqrt(1 - pow(absCosTheta,2));
      float sinThetaT = indexOfRefl * sinThetaI;
      if(sinThetaT > 1) { //Total internal reflection
        ProbablityOfReflection = 1;
      } else {
        float cosThetaT = sqrt(1 - pow(sinThetaT, 2));
        ProbablityOfReflection = Ro + (1-Ro)*pow((1-cosThetaT), 5);
      }
    }

    if(u0to1(rng) < ProbablityOfReflection) {
      //Making isReflective true and treating it as a reflective surface
      isReflective = 1;

    } else {
      
      //Other wise finding new direction using snell's law :- n1 sin(theta(i)) = n2 sin(theta(t))
      //2 cases, 1. Light entering new material, 2. light leaving new material( total intereal reflection may occur)
      float sinThetaI = sqrt(1 - pow(absCosTheta,2));
      float sinThetaT;
      float n1, n2;
      glm::vec3 actualNormal;

      //1. if light entering new material
      if(cosTheta < 0) { //normal and light direction are in opposite
        //Snell's law:- n1 sin(theta(i)) = n2 sin(theta(t))
        sinThetaT = sinThetaI / indexOfRefl; 
        n1 = 1;
        n2 = indexOfRefl;
        actualNormal = hitNormal; // SOLVED ERROR: Forgot this line inresult, complete object with flat it's color.
      } else {
      //2. if light leaving new material
        actualNormal = -hitNormal;
        sinThetaT = sinThetaI * indexOfRefl;
        if(sinThetaT > 1) { //Total internal reflection
          rayStatus = InteractionType::REFLECT; //NOTE::NEED TO CHECK THIS
          newDirection = getReflectedDirection(actualNormal, r.direction);
          return newDirection;
        }
        n1 = indexOfRefl;
        n2 = 1;
      }
      rayStatus = InteractionType::REFRECT;
      float n1BYn2 = n1/n2;
      // transmitted direction T  = I*(n1/n2) + ( cos(thetaI)*(n1/n2) - cos(thetaT)) * N
      newDirection = r.direction*n1BYn2 + (absCosTheta * n1BYn2 - sqrt(1-sinThetaT*sinThetaT)) * actualNormal;
      //float magnitute = glm::length(newDirection);
      return newDirection/*/magnitute*/;
    }
    //Statement reaches here if ray is reflecting
  }

  float specularity = m.specularity;

  ///For glossy reflection
  if(isReflective &&specularity>0){
    glm::vec3 newDir = getGlossyDirection(getReflectedDirection(hitNormal, r.direction), u0to1(rng), u0to1(rng),specularity);
    rayStatus = InteractionType::REFLECT;
    return glm::normalize(newDir);
  }
  
  ///PURE REFLECTION
  if(isReflective){
    rayStatus = InteractionType::REFLECT;
    //2 Possible normal values, 
    float cosTheta = glm::dot(r.direction, hitNormal); 
    float realNormal = 1;

    if(cosTheta > 0) {
      realNormal = -1;
    }

    //find reflective direction
    newDirection = getReflectedDirection(realNormal * hitNormal, r.direction);
    return newDirection;
  }
}




#endif //RAY_MATERIAL_H