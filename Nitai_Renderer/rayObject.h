#ifndef RAY_OBJECT_H
#define RAY_OBJECT_H

#include <glm\glm.hpp>
#include <cuda_runtime.h>

#include "structsWorld.h"
#include "rayMaterial.h"

//////FORWARD DECLARATION///////
__host__ __device__ float rayTriangleTest(glm::vec3 v0, glm::vec3 v1, glm::vec3 v2, ray r, glm::vec3 &inPoint, glm::vec3 &inNormal);

__host__ __device__ bool rayBBTest(ray &r, glm::vec3 minBB, glm::vec3 maxBB);

__host__ __device__ void swap(float &a, float &b) {
  a = a+b;
  b = a-b;
  a = a-b;
}


//////DEFINITIONS////////
//Cube intersection test, return -1 if no intersection, otherwise, distance to intersection
__host__ __device__ float rayBoxTest(object &box, const ray &r, glm::vec3 &intersectionPoint, glm::vec3 &normal) {
  //calculate ray eq. in local coordinate for box
  glm::vec4 tmp4 = box.invTransformation * glm::vec4(r.origin,1);
  glm::vec3 origin (tmp4.x, tmp4.y, tmp4.z) ;
  tmp4 = glm::normalize(box.invTransformation * glm::vec4(r.direction,0));
  glm::vec3 direction(tmp4.x, tmp4.y, tmp4.z);

  //assign box min and max in (0.5) range
  //glm::vec3 boxMin (-0.5, -0.5, -0.5);
  //glm::vec3 boxMax (0.5, 0.5, 0.5);
  float boxDim = 0.5;

//PART 1. Getting Tminimum and Tmaximum
  //X-Axis - calculate Tmin and Tmax for x-axis based on value
  float Tmin = -9999;
  float Tmax = 9999;

  float xDInverse = 1/direction.x;
  Tmin = (-boxDim - origin.x)*xDInverse;
  Tmax = (boxDim - origin.x)*xDInverse;
  if(Tmin>Tmax) swap(Tmax, Tmin);


  float yDInverse = 1/direction.y;
  float Tymin = (-boxDim - origin.y)*yDInverse;
  float Tymax = (boxDim - origin.y)*yDInverse;
  if(Tymin>Tymax) swap(Tymin, Tymax);
  
  //if Tymin>Tmax or Tmin>Tymax return -1
  if(Tymin>Tmax || Tmin>Tymax) {
    return -1;
  }

  //if Tymin>Tmin update Tmin and if Tymax<Tmax update Tmax
  if(Tymin>Tmin) Tmin = Tymin;
  if(Tymax<Tmax) Tmax = Tymax;

  float zDInverse = 1/direction.z;
  float Tzmin = (-boxDim - origin.z)*zDInverse;
  float Tzmax = (boxDim - origin.z)*zDInverse;
  if(Tzmin>Tzmax) swap(Tzmin, Tzmax);
  
  //if Tzmin>Tmax or Tmin>Tzmax return -1
  if(Tzmin>Tmax || Tmin>Tzmax) {
    return -1;
  }

  //if Tzmin>Tmin update Tmin and if Tzmax<Tmax update Tmax
  if(Tzmin>Tmin) Tmin = Tzmin;
  if(Tzmax<Tmax) Tmax = Tzmax;
  //}

  //if Tmax < 0 return -1
  if(Tmax<0) return -1;
  //if Tmin < 0 update Tmin to Tmax, This is for Refraction case
  if(Tmin<0) Tmin = Tmax;

//PART 2. getting object space point and normal for box
  glm::vec3 localNormal; //all valuse are 0,0,0 in constructor
  glm::vec3 localPoint = origin + Tmin*direction;


  if( (int)(fabs(localPoint.x)+0.501) ) {       //its on x-axis
      if(localPoint.x > 0){
      localNormal.x = 1;
      } else {
      localNormal.x = -1;
      } 
  } else if( (int)(fabs(localPoint.y)+0.501) ) { //its on y-axis
      if(localPoint.y > 0){
      localNormal.y = 1;
      } else {
      localNormal.y = -1;
      }
  } else {                                     //its on z-axis
      if(localPoint.z > 0){
      localNormal.z = 1;
      } else {
      localNormal.z = -1;
      }
  }

  //PART 3. getting world space point and normal
  //getting normal
  tmp4 = box.transformation * glm::vec4(localNormal,0);
  //UnNormalized:: worldNormal
  normal = glm::vec3(tmp4.x, tmp4.y, tmp4.z);

  tmp4 = box.transformation * glm::vec4(localPoint,1);
  intersectionPoint = glm::vec3(tmp4.x, tmp4.y, tmp4.z);

  glm::vec3 lenVector = r.origin-intersectionPoint;
  //return minimum positive T
  return glm::dot(lenVector, lenVector);
}
 
__host__ __device__ float raySphereTest(object &sph, const ray &r, glm::vec3 &intersectionPoint, glm::vec3 &normal) {

  float radiusSquare = 0.25;//Radius = 0.5

  //calculate ray eq. in local coordinate for box
  glm::vec4 tmp4 = sph.invTransformation * glm::vec4(r.origin,1);
  glm::vec3 origin (tmp4.x, tmp4.y, tmp4.z) ;
  tmp4 = sph.invTransformation * glm::vec4(r.direction,0);
  glm::vec3 direction = glm::normalize(glm::vec3(tmp4.x, tmp4.y, tmp4.z));

  //A(t)2 + B(t) + C = 0
  //A = (rd.rd)=1, B = (ro.rd), C = (ro.ro-(radi)2)
  float B = glm::dot(origin, direction);
  float C = glm::dot(origin, origin)-radiusSquare;

  float D = B*B - C;//discriminant

  if(D<0) { //D<0
    return -1;
  }
  float t1,t2;
  //if(D < 0.0000001) { //D=0
  //  t1 = -B;
  //} else {
  D = sqrt(D);
  B = -B;
  t1 = B + D;
  t2 = B - D;
  //}
  t1 = fminf(t1, t2);
  if(t2<0){//SOLVER ERROR:  forgot this in result got error regarding Normal
    return -1;
  }
  if(t1<0) { // for refraction case
    t1=t2;
  }
  //Calculating world intersection point
  glm::vec3 intersect = origin + t1*direction;
  tmp4 = sph.transformation * glm::vec4(intersect,1); //This now contain intersection point in world space
  intersectionPoint = glm::vec3(tmp4.x, tmp4.y, tmp4.z);

  //Calculating world Normal ::NOTE:: UnNormalized normal
  glm::vec4 center = sph.transformation[3];
  tmp4  =  tmp4 - center;
  normal = glm::vec3(tmp4.x, tmp4.y, tmp4.z);
  glm::vec3 lenVector = intersectionPoint-r.origin;
  return glm::dot(lenVector, lenVector);
}

__host__ __device__ float rayMeshTest(const GPUMesh *mesh, ray r, glm::vec3 &intersectionPoint, glm::vec3 &normal) {
  ray localR;
  glm::vec4 tmp4;
  tmp4 = mesh->invTransformation*glm::vec4(r.origin,1);
  localR.origin = glm::vec3(tmp4.x, tmp4.y, tmp4.z);
  tmp4 = glm::normalize(mesh->invTransformation*glm::vec4(r.direction,0));
  localR.direction = glm::vec3(tmp4.x, tmp4.y, tmp4.z);

  //first test with its bounding box
  if(!rayBBTest(localR, mesh->minBB, mesh->maxBB)) {
    return -1;
  }

  //If bounding box test sucessed, we need ray-triangle tests
  //loop through all triangles
  float minDistance = 10000;
  for(int f=0; f<mesh->numberOfFaces; f++) {
    //ray - triangle intersecion test
    face *currF = &(mesh->gpuFaces[f]);
    glm::vec3 localP;
    glm::vec3 localN;
    float currentDist = rayTriangleTest(mesh->gpuVerticies[currF->v0], mesh->gpuVerticies[currF->v1], mesh->gpuVerticies[currF->v2], localR, localP, localN); 
    //float currentDist =  rayTriangleTest1(r.origin, r.direction, mesh->gpuVerticies[currF->v0], mesh->gpuVerticies[currF->v1], mesh->gpuVerticies[currF->v2]);  

    //if there is a intersection
    if(currentDist>0) {
      //if currentDist is less then minDistance then update minDistance, point and normal
      if(currentDist<minDistance) {
        minDistance = currentDist;
        intersectionPoint = localP;
        //UnNormalized normal
        normal = localN;
      }
    }
  }
  if(epsilonCheck(minDistance,10000)) {
    return -1;
  }

  tmp4              = mesh->transformation * glm::vec4(intersectionPoint, 1);
  intersectionPoint = glm::vec3(tmp4.x, tmp4.y, tmp4.z);
  tmp4              = glm::normalize(mesh->transformation * glm::vec4(normal,0));
  normal            = glm::vec3(tmp4.x, tmp4.y, tmp4.z);

  glm::vec3 lenVector = r.origin - intersectionPoint; 
  return glm::dot(lenVector, lenVector);
}

__host__ __device__ float rayTriangleTest(glm::vec3 v0, glm::vec3 v1,  glm::vec3 v2,  ray r, glm::vec3 &inPoint, glm::vec3 &inNormal) {
  ///getting point in plane
  glm::vec3 a = v1 - v0;
  glm::vec3 b = v2 - v0;
  glm::vec3 normal = glm::cross(a,b);

  //Equation of plane is D => N.X - N.V0 = 0
  //So by solving eq with ray=> t = N.(V0-r.origin)/dot(N, r.direction);
  float denominator = glm::dot(normal,r.direction);
  if(abs(denominator)< 1e-20) { //I.E. ray is in parallel to triangle
    return -1;
  }

  float t = glm::dot(normal,(v0-r.origin))/denominator;

  if(t<0) { // triangle is back side of ray
    return -1;
  }
  glm::vec3 hitP = r.origin + t*r.direction;
  
  ///checking point in triangle
  //Theory:: any point is in triangle if dot((V1-V0)*P, N), dot((V2-V1)*P,N) and dot((V0-V2)*P, N) is positive, otherwise it is outside
  if(glm::dot(glm::cross((v1-v0),(hitP-v0)),normal) < 0) return -1;
  if(glm::dot(glm::cross((v2-v1),(hitP-v1)),normal) < 0) return -1;
  if(glm::dot(glm::cross((v0-v2),(hitP-v2)),normal) < 0) return -1;

  inPoint = hitP;
  //Note: this normal is not normalized
  inNormal = normal; 

  return t;
}

__host__ __device__ bool rayBBTest(ray &r, glm::vec3 minBB, glm::vec3 maxBB) {
  //glm::vec3 boxMin (-0.5, -0.5, -0.5);
  //glm::vec3 boxMax (0.5, 0.5, 0.5);
  glm::vec3 direction = r.direction;
  glm::vec3 origin = r.origin;

//PART 1. Getting Tminimum and Tmaximum
  //X-Axis - calculate Tmin and Tmax for x-axis based on value
  float Tmin = -9999;
  float Tmax = 9999;

  float xDInverse = 1/direction.x;
  Tmin = (minBB.x - origin.x)*xDInverse;
  Tmax = (maxBB.x - origin.x)*xDInverse;
  if(Tmin>Tmax) swap(Tmax, Tmin);


  float yDInverse = 1/direction.y;
  float Tymin = (minBB.y - origin.y)*yDInverse;
  float Tymax = (maxBB.y - origin.y)*yDInverse;
  if(Tymin>Tymax) swap(Tymin, Tymax);
  
  //if Tymin>Tmax or Tmin>Tymax return -1
  if(Tymin>Tmax || Tmin>Tymax) {
    return false;
  }

  //if Tymin>Tmin update Tmin and if Tymax<Tmax update Tmax
  if(Tymin>Tmin) Tmin = Tymin;
  if(Tymax<Tmax) Tmax = Tymax;

  float zDInverse = 1/direction.z;
  float Tzmin = (minBB.z - origin.z)*zDInverse;
  float Tzmax = (maxBB.z - origin.z)*zDInverse;
  if(Tzmin>Tzmax) swap(Tzmin, Tzmax);
  
  //if Tzmin>Tmax or Tmin>Tzmax return -1
  if(Tzmin>Tmax || Tmin>Tzmax) {
    return false;
  }

  //if Tzmin>Tmin update Tmin and if Tzmax<Tmax update Tmax
  if(Tzmin>Tmin) Tmin = Tzmin;
  if(Tzmax<Tmax) Tmax = Tzmax;

  //if Tmax < 0 return -1
  if(Tmax<0) return false;

  return true;
}

#endif //RAY_OBJECT_H