//Hare Ram Hare Ram
//Ram Ram Hare Hare

uniform sampler2D tex;

in vec2 fsUV;

void main() {
  vec4 color = texture2D(tex, fsUV);
  gl_FragColor = vec4(color.x, color.y, color.z ,1); /*+ vec4(0.7, 0.7, 0.7,1);*/ //vec4(1,0,1,1);
}