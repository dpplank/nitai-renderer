#pragma once
#include "scene.h"
#include <cuda.h>
#include "cuda_runtime.h"

class simplePathTracer
{
public:
  simplePathTracer(void);
  ~simplePathTracer(void);
  void setScene(scene *s);
  void render(uchar4* pbo, int frameNo);
  
};

