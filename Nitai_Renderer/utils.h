#pragma once
#include <rapidjson\document.h>
#include <cuda_runtime.h>
#include <glm\glm.hpp>

#define PI 3.14159265358979323846264338327950f


#define CUDA_ERROR(error) utils::checkCUDAError(error, __FILE__, __LINE__);
#define CUDA_LAST_ERROR utils::checkCUDAError(cudaGetLastError(), __FILE__, __LINE__);
#define OPENGL_ERROR utils::checkOpenglError(__FILE__, __LINE__);

class utils
{
public:
  utils(void);
  ~utils(void);

  static char *getFileToChar(const char *fileName);
  static void checkJsonError(rapidjson::ParseErrorCode code, char* file, int line);

  static void checkCUDAError(cudaError_t error, char *file, int line);
  static void checkOpenglError(char *file,int line);

  static float getRandom(float seed);

  static glm::mat4 getRotateMatrix(glm::vec3 axis, float angleInTheta);

  static glm::vec3 getRotation(glm::vec3 vec, glm::vec3 axis, float angleInDegree);

  static void createImage(glm::vec3 *imageData, int width, int height, const char *fileName);

  static int clampi(int val, int low, int high); 
};

