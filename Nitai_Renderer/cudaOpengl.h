#pragma once
#include <GL/glew.h>
#include <cuda_runtime.h>

class cudaOpengl
{
public:
  cudaOpengl(void);
  ~cudaOpengl(void);
  void init(int argc, char **argv, int inWidth, int inHeight );

  ///GLUT callable function
  void setDisplayFunc(void (*func)(void));
  void setMouseFunc(void(*func)(GLint button, GLint state, GLint x, GLint y));
  void setMotionFunc(void(*func)(GLint x, GLint y));
  void setSpecialKeyFunc(void(*func)(int key, int x, int y));
  void setSpecialKeyUpFunc(void(*func)(int key, int x, int y));
  void setKeyboardFunc(void(*func)(unsigned char key, int x, int y));
  void startLoop();

  ///Binding functions
  void bindPBO();
  void bindTexture();
  void bindIndex();

  ///Opengl Callable functions
  void draw();
  void swapBuffer();

private:
  void initGlut(int argc, char **argv);
  void initGL();
  void initPboCuda();

  void createShader(const char *vShader, const char *fShader);

  //static void runCUDA();

public:
  ///Cuda-OpenGL
  cudaGraphicsResource *pboCUDAResource;
  GLuint pbo;
private:
  GLuint bufIdx;
  GLuint bufPos;
  GLuint bufUV;

  GLint vPos, vUV;
  GLuint textureID;

  //widht and height comes from camera
  int width, height;

};

