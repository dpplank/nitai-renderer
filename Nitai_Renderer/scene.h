#pragma once
#include <rapidjson\document.h>
#include <vector>
#include <string>
#include "structsWorld.h"


class scene
{
public:
  scene(void);
  ~scene(void);
  void createSceneFromFile(const char *fileName);

private:
  void createMaterials(rapidjson::Value &v);
  void createObjects(rapidjson::Value &v);
  void createCamera(rapidjson::Value &v);
  bool createMeshFromFile(std::string fileName, mesh *inMesh);//return true if sucessful else false

///Data Members
public:
  std::vector<material> materials;
  std::vector<object> objects;
  std::vector<mesh *> meshes;
  camera cam;
};

