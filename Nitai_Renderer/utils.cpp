#include "utils.h"
#include <fstream>
#include <iostream>
#include <GL/glew.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>

#include <stb/stb_image_write.h>


utils::utils(void)
{
}


utils::~utils(void)
{
}

char *utils::getFileToChar(const char *fileName) {
  std::ifstream inFile(fileName);
	/* std::vector<char> buffer; This are alternative way on doing this but it through some problem in compiling
	buffer.clear();
	buffer.reserve(500); */
  if(!inFile.is_open()) {
    std::cout<<"ERROR: File path is invalid!"<<std::endl;
    system("pause");
  }

	//inFile.seekg(0, inFile.end);
	int counter;
	//counter = inFile.tellg();
	for (counter = 0; inFile.get() != EOF; counter++); 
	inFile.clear(); //clear EOF state
	inFile.seekg(0, inFile.beg); //return to begining of file
	char *bufferChar = new char[counter+1];

	inFile.read(bufferChar, counter);
	/*while(!inFile.eof()) {
		c=inFile.get();
		buffer.push_back(c);
	}
	for(int i=0; i< buffer.size();  i++) {
		bufferChar[i] = buffer[i];
	}*/

	bufferChar[counter] = '\0';
	
	inFile.close();
	return bufferChar;
}

void utils::checkJsonError(rapidjson::ParseErrorCode code, char* file, int line) {
 
  if(code == rapidjson::kParseErrorNone) {
    //No error
    return;
  }

  //get only file name out of long path
  char c = '\\';
  char *fileName = strrchr(file,c);

  std::cerr<<"Error occured in: "<< fileName<< " at line: "<< line<< " : ";

  switch (code)
  {
  case rapidjson::kParseErrorDocumentEmpty:
    std::cerr<<"kParseErrorDocumentEmpty"<<std::endl;
    break;
  case rapidjson::kParseErrorDocumentRootNotSingular:
    std::cerr<<"kParseErrorDocumentRootNotSingular"<<std::endl;
    break;
  case rapidjson::kParseErrorValueInvalid:
    std::cerr<<"kParseErrorValueInvalid"<<std::endl;
    break;
  case rapidjson::kParseErrorObjectMissName:
    std::cerr<<"kParseErrorObjectMissName"<<std::endl;
    break;
  case rapidjson::kParseErrorObjectMissColon:
    std::cerr<<"kParseErrorObjectMissColon"<<std::endl;
    break;
  case rapidjson::kParseErrorObjectMissCommaOrCurlyBracket:
    std::cerr<<"kParseErrorObjectMissCommaOrCurlyBracket"<<std::endl;
    break;
  case rapidjson::kParseErrorArrayMissCommaOrSquareBracket:
    std::cerr<<"kParseErrorArrayMissCommaOrSquareBracket"<<std::endl;
    break;
  case rapidjson::kParseErrorStringUnicodeEscapeInvalidHex:
    std::cerr<<"kParseErrorStringUnicodeEscapeInvalidHex"<<std::endl;
    break;
  case rapidjson::kParseErrorStringUnicodeSurrogateInvalid:
    std::cerr<<"kParseErrorStringUnicodeSurrogateInvalid"<<std::endl;
    break;
  case rapidjson::kParseErrorStringEscapeInvalid:
    std::cerr<<"kParseErrorStringEscapeInvalid"<<std::endl;
    break;
  case rapidjson::kParseErrorStringMissQuotationMark:
    std::cerr<<"kParseErrorStringMissQuotationMark"<<std::endl;
    break;
  case rapidjson::kParseErrorStringInvalidEncoding:
    std::cerr<<"kParseErrorStringInvalidEncoding"<<std::endl;
    break;
  case rapidjson::kParseErrorNumberTooBig:
    std::cerr<<"kParseErrorNumberTooBig"<<std::endl;
    break;
  case rapidjson::kParseErrorNumberMissFraction:
    std::cerr<<"kParseErrorNumberMissFraction"<<std::endl;
    break;
  case rapidjson::kParseErrorNumberMissExponent:
    std::cerr<<"kParseErrorNumberMissExponent"<<std::endl;
    break;
  case rapidjson::kParseErrorTermination:
    std::cerr<<"kParseErrorTermination"<<std::endl;
    break;
  case rapidjson::kParseErrorUnspecificSyntaxError:
    std::cerr<<"kParseErrorUnspecificSyntaxError"<<std::endl;
    break;
  default:
    break;
  }

  system("pause");

}

void utils::checkCUDAError(cudaError_t error, char *file, int line) {
  if( cudaSuccess != error) {
    //Getting only file name instead of full path
    char c = '\\';
    char * index = strrchr(file, c);
    if(index==nullptr){ //In cuda file (".cu") we get path with '/' seperations
      c='/';
      index = strrchr(file,c);
    }
    index++;

    fprintf(stderr, "Cuda error: In file\"%s\": at line \"%i\":: %s: %s.\n", index, line, cudaGetErrorName(error), cudaGetErrorString( error) ); 
    system("pause");
    exit(EXIT_FAILURE); 
  }
}

void utils::checkOpenglError(char *file,int line) {
  char c = '\\';
  char * index = strrchr(file, c);
  index++;
  
  GLenum error =  glGetError();

  while(error != GL_NO_ERROR) {
    switch( error ) {
    case (GL_INVALID_ENUM) : {
      printf("GL_INVALID_ENUM, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case (GL_INVALID_VALUE) : {
      printf("GL_INVALID_VALUE, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_INVALID_OPERATION): {
      printf("GL_INVALID_OPERATION, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_INVALID_FRAMEBUFFER_OPERATION): {
      printf("GL_INVALID_FRAMEBUFFER_OPERATION, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_OUT_OF_MEMORY): {
      printf("GL_OUT_OF_MEMORY, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_STACK_UNDERFLOW): {
      printf("GL_STACK_UNDERFLOW, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_STACK_OVERFLOW): {
      printf("GL_STACK_OVERFLOW, at line no. %d, in flie %s\n", line, index);
      break;
      }
   default: {
        printf("UNSPECIFED ERROR, at line no. %d, in flie %s\n", line, index);
      }
    }
    system("pause");
    error =  glGetError();
  }

}

float utils::getRandom(float seed){ 
  srand(seed);
  return rand()/(float)RAND_MAX;
}

glm::mat4 utils::getRotateMatrix(glm::vec3 axis, float angleInTheta) {
  float a=axis.x, b=axis.y, c=axis.z;
  float theta = angleInTheta* PI / 180; //angle in Radian
  glm::mat4 R = glm::mat4(
    a*a + (1 - a*a)*cos(theta),         a*b*(1-cos(theta)) - c*sin(theta),  a*c*(1-cos(theta)) + b*sin(theta), 0,
    a*b*(1-cos(theta)) + c*sin(theta),  b*b + (1 - b*b)*cos(theta),         b*c*(1-cos(theta)) - a*sin(theta), 0,
    a*c*(1-cos(theta)) - b*sin(theta),  b*c*(1-cos(theta)) + a*sin(theta),  c*c + (1 - c*c)*cos(theta)       , 0,
    0                                ,  0                                ,  0                                , 1
    );

  //because glm stores in column major order need transpose
  return glm::transpose(R);
}

glm::vec3 utils::getRotation(glm::vec3 vec, glm::vec3 axis, float angleInDegree) {
  float a = axis[0], b = axis[1], c= axis[2];
  float theta = angleInDegree * PI / 180; //angle In radian
  glm::mat3 R = glm::mat3(
    a*a + (1 - a*a)*cos(theta),         a*b*(1-cos(theta)) - c*sin(theta),  a*c*(1-cos(theta)) + b*sin(theta),
    a*b*(1-cos(theta)) + c*sin(theta),  b*b + (1 - b*b)*cos(theta),         b*c*(1-cos(theta)) - a*sin(theta),
    a*c*(1-cos(theta)) - b*sin(theta),  b*c*(1-cos(theta)) + a*sin(theta),  c*c + (1 - c*c)*cos(theta) 
    );

  //Because glm stores Column major matirix
  R = glm::transpose(R);

  return R*vec;
}

void utils::createImage(glm::vec3 *imageData, int width, int height, const char *fileName) {
  struct imageByte{ unsigned char r, g, b;};

  imageByte *imageBMP = new imageByte[width *height];
  int totalPixels = width*height;

  float gamma = 1/2.2;
  bool gammaEnable=true;

  //NOTE:::: in imageData, the image starts from left-bottom, but in imageBMP we have to fill from top-left
  for(int i=0; i<height; i++) { //Y
    for(int j=0; j<width; j++) { //X
      int indexFrom = i*width + j;
      int indexTo   = (height-i-1)*width+j;
      if(gammaEnable) {
        imageBMP[indexTo].r = clampi(pow(imageData[indexFrom].r, gamma)*255,0,255 );
        imageBMP[indexTo].g = clampi(pow(imageData[indexFrom].g, gamma)*255,0,255 );
        imageBMP[indexTo].b = clampi(pow(imageData[indexFrom].b, gamma)*255,0,255 );
      } else {
        imageBMP[indexTo].r = clampi(imageData[indexFrom].r*255,0,255 );
        imageBMP[indexTo].g = clampi(imageData[indexFrom].g*255,0,255 );
        imageBMP[indexTo].b = clampi(imageData[indexFrom].b*255,0,255 );
      }
    }
  }

  stbi_write_bmp(fileName, width, height, 3, imageBMP); 
}

int utils::clampi(int val, int low, int high) {
  if(val<low) return low;
  if(val>high) return high;
  return val;

}
