#include <cuda_runtime.h>
#include <thrust/random.h>
#include <stdio.h>
#include <glm\glm.hpp>
#include <thrust\device_ptr.h>
#include <device_functions.h>

#include "structsWorld.h"
#include "rayHandler.h"
#include "scene.h"
#include "cameraHandler.h"
#include "utils.h"

#include <thrust\scan.h>
#include <thrust\copy.h>
#include <thrust\execution_policy.h>


#define DEBUG 0


__global__ void initializeImage(glm::vec3 *image, int width, int height) {
  int x = (blockIdx.x * blockDim.x) + threadIdx.x;
  int y = (blockIdx.y * blockDim.y) + threadIdx.y;

  if(x<width && y<height) {
    int index = y*width + x;
    image[index] = glm::vec3(1,1,1);
  }

}

__global__ void mergeIntoPBO(uchar4 *pbo, glm::vec3 *image, glm::vec3 *localImage, int frameCount, float width, float height) {
   int x = (blockIdx.x * blockDim.x) + threadIdx.x;
  int y = (blockIdx.y * blockDim.y) + threadIdx.y;
  int index = x + (y * width);
  
  if(x<width && y<height){
      //calculate final image
      glm::vec3 finalImage = (image[index]*(float)(frameCount-1) + localImage[index])/ (float)frameCount;
      image[index] = finalImage;

      uchar4 pboVal;

      pboVal.x = fminf(finalImage.x*255,255);
      pboVal.y = fminf(finalImage.y*255,255);
      pboVal.z = fminf(finalImage.z*255,255);
      pboVal.w = 1;

      pbo[index] = pboVal;      
  }
}

__global__ void randomColor(uchar4 *pbo, int width, int height, float iter) {
  int x = blockDim.x * blockIdx.x + threadIdx.x;
  int y = blockDim.y * blockIdx.y + threadIdx.y;

  if(x < width && y < height) {
    int index = y*width + x;

    thrust::default_random_engine rand(hash(index*iter));
    thrust::uniform_real_distribution<float> ur0to1(0, 1);

    glm::vec3 color (ur0to1(rand), ur0to1(rand), ur0to1(rand));
    color *= 1;

    if(x<100 && y<100) {
      pbo[index].x = 0;//color.x;
      pbo[index].y = 0;
      pbo[index].z = 0;
      pbo[index].w = 1;
    } else {
      pbo[index].x = color.x*255;//color.x;
      pbo[index].y = color.y*255;
      pbo[index].z = color.z*255;
      pbo[index].w = 1;
    }
  }
}


__global__ void pathTracer(rayPoolElement *rpe, int rayPoolSize, object* geoms, int numberOfGeoms, GPUMesh mesh, int numberOfMeshes, material *materials, int numberOfMaterials, 
                           glm::vec3* colors, float iteration, int bounces) 
{

  // check for wheter current thread is less then size of ray pool
  int row = blockIdx.x * blockDim.x + threadIdx.x;
 
  if(row < rayPoolSize)  {
    //find nearest intersection and object
    glm::vec3 point;
    glm::vec3 normal;
    object geometry;
    bool hit = giveIntersection(rpe[row].r, geoms, numberOfGeoms, &mesh, numberOfMeshes, &point, &normal, &geometry);

    //if no object hit, make current ray invalid and return black
    if(!hit) {
      rpe[row].isValid = false;
      //rpe[row].r.direction = glm::vec3(0,0,0);

      //Below for black background
      //colors[rpe[row].index] *= glm::vec3(0,0,0);
      //return;

      //NEVER REMOVE CODE BELOW:: Code below will give sky like effect on background
      glm::vec3 unit_v = rpe[row].r.direction;
      const float a = 0.5 * (unit_v.y+1);
      colors[rpe[row].index] *= (1.0f-a) * glm::vec3(1,1,1) + a * glm::vec3(0.5,0.7,1.0);

      return;

    }
    if (materials[geometry.materialIndex].emittance > 0 ) {
    //else if light hit, make current ray invalid and return light emmitance
      rpe[row].isValid=false;
      //rpe[row].r.direction = glm::vec3(0,0,0);
      colors[rpe[row].index] *= materials[geometry.materialIndex].color * materials[geometry.materialIndex].emittance; 
      return;
    }

    //if(!DEBUG) {
    //  rpe[row].isValid=false;
    //  glm::vec3 currentColor = colors[rpe[row].index];
    //  currentColor *= materials[geometry.materialIndex].color;
    //  colors[rpe[row].index] = currentColor;
    //  return;
    //}

    InteractionType rayStatus;
    glm::vec3 newDirection = getMyBSDF(rpe[row].r, point, normal, materials[geometry.materialIndex], rayStatus, iteration, bounces, row);

    //make current ray origin as intersected point and direction as above random direction
    //IMP NOTE:= here multiplying factor with newDirection, may cause outer ring in Transparent materials. Change that factor to see cool ring effect in 
    // transparent glass material. more it reduces, more bigger outer ring. EX:- 0.001, 0.0001, etc.
    rpe[row].r.origin = point + 0.01f*newDirection; //SOLVED ERROR:= I was using very small value for multipying factor.
    rpe[row].r.direction = newDirection;

    //get geometry color and multiply it with current color
    if(rayStatus==InteractionType::SCATTER || rayStatus==InteractionType::REFRECT) {
    glm::vec3 currentColor = colors[rpe[row].index];
    currentColor *= materials[geometry.materialIndex].color;
    colors[rpe[row].index] = currentColor;
    }
  }
}



//a structure for stream compaction on ray pool elements
template <typename T>
struct validRays {
  __host__ __device__
  bool operator() (const T &rpe) {    
    return rpe.isValid;
  }
};


void renderFrame(uchar4 *pbo, scene &world, int frameCount) {
  float width = world.cam.res.x;
  float height = world.cam.res.y;

  dim3 dimBlock (8, 8);
  dim3 dimGrid (ceil(width/(float)dimBlock.x), ceil(height/(float)dimBlock.y));
  //randomColor<<<dimGrid, dimBlock>>>(pbo, width, height, frameCount);


  //////PREPARING COLOR HOLDERS//////
  int pixelCount = width*height;
  int pixelByte = pixelCount*sizeof(glm::vec3);

  glm::vec3 *image=NULL;
  cudaMalloc((void**)&image, pixelByte);
  cudaMemcpy(image, world.cam.image, pixelByte, cudaMemcpyHostToDevice);

  glm::vec3 *localImage=NULL;
  cudaMalloc((void**)&localImage, pixelByte);
  initializeImage<<<dimGrid, dimBlock>>>(localImage, width, height);

  
  ///////FILLING RAY POOLS///////
  //create memory for RayPoolElement (RPE): each RPE represent color of one pixel
  rayPoolElement *rpe = NULL;
  cudaMalloc((void**)&rpe, sizeof(rayPoolElement)*width*height);
  createRayPool<<<dimGrid, dimBlock>>>(world.cam, rpe, frameCount);


  //////RENDER PREPERATION////////////
  material *cudaMaterial=NULL;
  int materialBytes = world.materials.size()*sizeof(material);
  cudaMalloc((void**)&cudaMaterial, materialBytes);
  cudaMemcpy(cudaMaterial, (world.materials.data()), materialBytes, cudaMemcpyHostToDevice);

  object *cudaObject = NULL;
  int objectBytes = world.objects.size()*sizeof(object);
  cudaMalloc((void**)&cudaObject, objectBytes);
  cudaMemcpy(cudaObject, (world.objects.data()), objectBytes, cudaMemcpyHostToDevice); 

  ///Mesh preperation
  GPUMesh gpuMesh;

  //Currently support only one mesh
  if(world.meshes.size()>0) {
    glm::vec3 *cudaVerts = NULL;
    face *cudaFaces = NULL;
    int vertsBytes =  world.meshes[0]->verticies.size()*sizeof(glm::vec3);
    int facesBytes = world.meshes[0]->faces.size()*sizeof(face);
    cudaMalloc((void**)&cudaVerts, vertsBytes);
    cudaMalloc((void**)&cudaFaces, facesBytes);
    cudaMemcpy(cudaVerts, world.meshes[0]->verticies.data(), vertsBytes, cudaMemcpyHostToDevice);
    cudaMemcpy(cudaFaces, world.meshes[0]->faces.data(), facesBytes, cudaMemcpyHostToDevice);

    gpuMesh.gpuVerticies      = cudaVerts;
    gpuMesh.numberOfVerts     = world.meshes[0]->verticies.size();
    gpuMesh.gpuFaces          = cudaFaces;
    gpuMesh.numberOfFaces     = world.meshes[0]->faces.size();
    gpuMesh.type              = world.meshes[0]->type;
    gpuMesh.materialIndex     = world.meshes[0]->materialIndex;
    gpuMesh.transformation    = world.meshes[0]->transformation;
    gpuMesh.invTransformation = world.meshes[0]->invTransformation;
    gpuMesh.minBB             = world.meshes[0]->minBB;
    gpuMesh.maxBB             = world.meshes[0]->maxBB;
  }
  ///////RENDER CURRENT FRAME/////////
  int bounces = 0;
  unsigned int size = pixelCount;
  const unsigned int traceDepth = 5; //Determine how many bounces the tracer has

  //CUDA_LAST_ERROR
  //CUDA_ERROR(cudaDeviceSynchronize())

  //PROBLEM:: We are getting size ==1 after some iteration which is actually big problem... Need to resolve this
  CUDA_LAST_ERROR
  while(size && (bounces < traceDepth)) {
    /// i. Launch a 1-D kernel for ray-tracing, rays also responsible for ovrriding their ray with reflected one, if exists 
    //  In kernel, as parameter , also send size of array
    dim3 threadsPerBlock1D(32);
    dim3 blocksPerGrid1D ((int)ceil((float)size / (float)32));
    pathTracer<<<blocksPerGrid1D, threadsPerBlock1D>>> (rpe, size, cudaObject, world.objects.size(), gpuMesh, world.meshes.size(), cudaMaterial, world.materials.size(), localImage,(float)frameCount, bounces);
    //allocate new memory to hold new ray pool elements
    rayPoolElement *tmpRPE;
    cudaMalloc((void**)&tmpRPE, size*sizeof(rayPoolElement));

    //allocate raw pointers for rayPoolElements so that we can use thrust algorithms, like std::iterators
    thrust::device_ptr<rayPoolElement> rpePointer(rpe);
    thrust::device_ptr<rayPoolElement> tmpRpePointer(tmpRPE);

    validRays<rayPoolElement> validation;
    //run thurst copy_if for stream compaction on old ray pool elements and allocate them in new above one
    thrust::device_ptr<rayPoolElement> inPointer = thrust::copy_if(thrust::device, rpePointer, rpePointer+size, tmpRpePointer, validation);
    cudaThreadSynchronize();

    //assign size as returned iterator - current iterator for new allocated memory
    size = inPointer - tmpRpePointer;

    //delete old ray pool and assign new ray pool to old one :) 
    cudaFree(rpe);
    rpe = tmpRPE;
    bounces++;

    CUDA_ERROR(cudaDeviceSynchronize())
    CUDA_LAST_ERROR
    
  }


  if(!DEBUG) {
    //get value of local image in array to analyze


  }


  //////MEARGING LOCAL IMAGE WITH PBO & GLOBAL IMAGE///////
  mergeIntoPBO<<<dimGrid, dimBlock>>>(pbo, image, localImage, frameCount, width, height);
  cudaMemcpy(world.cam.image, image, pixelByte, cudaMemcpyDeviceToHost); 

  /////FREEING MEMORY
  //MUST FREE ALL MEMEORY 
  cudaFree(rpe);
  cudaFree(image);
  cudaFree(localImage);
  //Need to update below for multiple meshes
  if(world.meshes.size()>0) {
    cudaFree(gpuMesh.gpuVerticies);
    cudaFree(gpuMesh.gpuFaces);
  }


  CUDA_LAST_ERROR
}





  //if(DEBUG) {
  //  uchar4 debugVal[500 * 500];// = new uchar4[width*height];
  //  cudaMemcpy(debugVal, pbo, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost);

  //  for(int i=0; i<width*height; i++) {
  //    //printf("value is: %d %d %d\n", debugVal[i].x, debugVal[i].y, debugVal[i].z);
  //  }
  //  printf("\n\n\n Finished!!");
  //}








