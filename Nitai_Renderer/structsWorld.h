#ifndef STRUCTS_WORLD
#define STRUCTS_WORLD

#include <glm/glm.hpp>
#include <cuda_runtime.h>
#include <vector>

#include "cudaUtils.h"

enum objectType { SPEHRE, CUBE, MESH };

#ifndef PI
#define PI 3.14159265358979323846264338327950f
#endif


struct ray {
  __host__ __device__ ray(glm::vec3 inOrigin, glm::vec3 inDirection):origin(inOrigin),direction(inDirection){}
  __host__ __device__ ray(){};
  glm::vec3 origin;
  glm::vec3 direction;
};

struct rayPoolElement {
  ray r;
  //pixel index, which this ray is coloring
  int index;
  //To validate the ray
  bool isValid;
};

struct camera {
    //phi :- angle from x-z plane
    //theta :- angle from y-z plane

    float theta, phi;
    float zoom;
    glm::vec3 ref; // view reference for paning of camera

    // for projection matrix
    float fovy;
    glm::vec2 res;
    //float nearP;

    //Image: represent latest rendered image in 0 to 1 format
    glm::vec3 *image;

    //Derived quantity
    glm::vec3 position;
    glm::vec3 up, view, right;

    int totalSamples;

    //Depth of field variables
    float aperture, focalDistance;
    bool isDOFEnable;
};

struct material {
  glm::vec3 color;
  glm::vec3 specColor;
  bool isReflect;
  bool isRefrect;
  float specularity;
  float indexOfRefraction;
  float emittance;
};

struct object {
  objectType type;
  int materialIndex;
  //glm::vec3 translation;
  //glm::vec3 rotation;
  //glm::vec3 scale;
  glm::mat4 transformation;
  glm::mat4 invTransformation;
};

struct face{
  face(int inV0, int inV1, int inV2):v0(inV0), v1(inV1), v2(inV2){}
  int v0, v1, v2;
};

//TODO:: Make a bounding box and fill it while creating
struct mesh: public object {
  std::vector<glm::vec3> verticies;
  std::vector<face> faces;
  glm::vec3 minBB;//Xmin, Ymin, Zmin
  glm::vec3 maxBB;//Xmax, Ymax, Zmax
};

struct GPUMesh: public object{
  glm::vec3 *gpuVerticies;
  int numberOfVerts;
  face *gpuFaces;
  int numberOfFaces;
  glm::vec3 minBB;
  glm::vec3 maxBB;
};


#endif //STRUCTS_WORLD