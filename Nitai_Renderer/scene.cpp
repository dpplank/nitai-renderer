#define GLM_SWIZZLE
#include "scene.h"
#include <rapidjson\document.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "utils.h"
#include <glm/gtc/matrix_transform.hpp>

#define DEBUG 1

scene::scene(void) {
}

scene::~scene(void) {
  //delete camera's image
  delete []cam.image;
}

void scene::createSceneFromFile(const char *fileName) {
  char * fileData = utils::getFileToChar("../scene/cornellBox.json");
  rapidjson::Document doc;
  doc.Parse(fileData);
  rapidjson::ParseErrorCode errorCode = doc.GetParseError();
  //NOTE: in RELEASE mode, debugger still stop at break point, and in that case __FILE__ and __LINE__ getting weird values in funtion.
  utils::checkJsonError(errorCode, __FILE__, __LINE__);

  if(doc.HasMember("hare krishna")) {
    std::cout<<"value of hare is: "<<doc["hare krishna"].IsInt()<<std::endl;
  }
  if(doc.HasMember("val")) {
    rapidjson::Value &v = doc["val"];
    std::cout<<"value 1 is: "<<v["val2"].GetInt()<<std::endl;
  }

  if(doc.HasMember("MATERIALS")) {
    createMaterials(doc["MATERIALS"]);
  }

  if(doc.HasMember("OBJECTS")) {
    createObjects(doc["OBJECTS"]);
  }

  if(doc.HasMember("CAMERA")){
    createCamera(doc["CAMERA"]);
  }
}

void scene::createMaterials(rapidjson::Value &v) {
  //Iterate through all materials
  for(rapidjson::Value::MemberIterator itr = v.MemberBegin(); itr != v.MemberEnd(); itr++) {
    std::cout<<"value of materials is : "<<itr->value["comment"].GetString()<<std::endl;
    //create new material
    material *mat = new material;

    //fill color
    if(itr->value.HasMember("RGB")) {
      rapidjson::Value &rgb = itr->value["RGB"];
      mat->color = glm::vec3(rgb[0].GetFloat(), rgb[1].GetFloat(), rgb[2].GetFloat());
    } else {
      mat->color = glm::vec3(1,1,1);
    }

    //fill specular color
    if(itr->value.HasMember("SPECRGB")) {
      rapidjson::Value &specRgb = itr->value["SPECRGB"];
      mat->specColor = glm::vec3(specRgb[0].GetFloat(), specRgb[1].GetFloat(), specRgb[2].GetFloat());
    } else {
      mat->specColor = glm::vec3(1,1,1);
    }

    //fill is Reflective
    if(itr->value.HasMember("REFLEC")) {
      mat->isReflect = itr->value["REFLEC"].GetBool();
    } else {
      mat->isReflect = false;
    }

    //fill is Refractive
    if(itr->value.HasMember("REFREC")) {
      mat->isRefrect = itr->value["REFREC"].GetBool();
    } else {
      mat->isRefrect = false;
    }

    //fill specularity, if available
    if(itr->value.HasMember("SEPCULARITY")) {
      mat->specularity = itr->value["SEPCULARITY"].GetFloat();
    } else {
      mat->specularity = 0;
    }

    //fill index of Refrection
    if(itr->value.HasMember("IDXREFREC")) {
      mat->indexOfRefraction = itr->value["IDXREFREC"].GetFloat();
    } else {
      mat->indexOfRefraction = 1;
    }

    //fill emmitance
    if(itr->value.HasMember("EMITTANCE")) {
      mat->emittance = itr->value["EMITTANCE"].GetFloat();
    } else {
      mat->emittance = 1;
    }
    materials.push_back(*mat);
    delete mat;
    int debug;
    debug=3;
  }

}

void scene::createObjects(rapidjson::Value &v) {
 //Iterate through all objects
  for(rapidjson::Value::MemberIterator itr = v.MemberBegin(); itr != v.MemberEnd(); itr++) {
    //create new object
    object *obj;

    //fill type
    assert(itr->value.HasMember("TYPE"));
    std::string str = itr->value["TYPE"].GetString();
    if(str == "SPHERE") {
      obj = new object;
      obj->type = objectType::SPEHRE;
    } else if(str == "CUBE") {
      obj = new object;
      obj->type = objectType::CUBE;
    } else if(str == "MESH") {
      obj = new mesh;
      obj->type = objectType::MESH;
      assert(itr->value.HasMember("FILE"));
      if( !createMeshFromFile(itr->value["FILE"].GetString(), (mesh*)obj) ) {
        //Obj not loaded correctly, skipping this file 
        delete obj;
        continue;
      }
    }

    //fill materialId
    assert(itr->value.HasMember("MATERIAL"));
    obj->materialIndex = itr->value["MATERIAL"].GetInt();

    //fill transformation
    assert(itr->value.HasMember("TRANS"));
    rapidjson::Value &transR = itr->value["TRANS"];
    assert(itr->value.HasMember("ROT"));
    rapidjson::Value &rotR = itr->value["ROT"];
    assert(itr->value.HasMember("SCALE"));
    rapidjson::Value &scaleR = itr->value["SCALE"];

    glm::vec3 trans (transR[0].GetFloat(), transR[1].GetFloat(), transR[2].GetFloat()); 
    glm::vec3 rot (rotR[0].GetFloat(), rotR[1].GetFloat(), rotR[2].GetFloat()); 
    glm::vec3 sca (scaleR[0].GetFloat(), scaleR[1].GetFloat(), scaleR[2].GetFloat()); 

    glm::mat4 rotation = utils::getRotateMatrix(glm::vec3(1,0,0),rot.x);
    rotation = utils::getRotateMatrix(glm::vec3(0,1,0), rot.y) * rotation;
    rotation = utils::getRotateMatrix(glm::vec3(0,0,1), rot.z) * rotation;

    //storing in column major order
    glm::mat4 translate (1, 0, 0, 0,
                         0, 1, 0, 0,
                         0, 0, 1, 0,
                         trans.x, trans.y, trans.z, 1);
    glm::mat4 scale ( sca.x, 0      , 0      , 0,
                      0      , sca.y, 0      , 0,
                      0      , 0      , sca.z, 0,
                      0      , 0      , 0      , 1);

    //1. Scale, 2. Rotation, 3. Translation
    obj->transformation = translate * rotation * scale;

    //fill inverse transformation
    obj->invTransformation = glm::inverse(obj->transformation);

    if(obj->type == objectType::MESH) {
      mesh *mLocal = (mesh*)obj;
      meshes.push_back(mLocal);
      //we will not delete mesh, because it is stores as pointer
    } else {
      objects.push_back(*obj);
      delete obj;
    }
  }
}

void scene::createCamera(rapidjson::Value &v) {
  //fill resolution
  assert(v.HasMember("RES"));
  cam.res.x = v["RES"][0].GetInt();
  cam.res.y = v["RES"][1].GetInt();

  //fill Fovy
  assert(v.HasMember("FOVY"));
  cam.fovy = v["FOVY"].GetFloat();

  //fill radius
  assert(v.HasMember("RADIUS"));
  cam.zoom = v["RADIUS"].GetFloat();

  //fill theta
  assert(v.HasMember("THETA"));
  cam.theta = v["THETA"].GetFloat();

  //fill phi
  assert(v.HasMember("PHI"));
  cam.phi = v["PHI"].GetFloat();

  //fill Ref
  if(v.HasMember("REF")) {
    cam.ref = glm::vec3(v["REF"][0].GetFloat(), v["REF"][1].GetFloat(), v["REF"][2].GetFloat());
  } else {
    cam.ref = glm::vec3(0,0,0);
  }

  if(v.HasMember("SAMPLES")) {
    cam.totalSamples = v["SAMPLES"].GetInt();
  } else {
    cam.totalSamples = 10000;
  }

  if(v.HasMember("ISDOF")) {
    cam.isDOFEnable = v["ISDOF"].GetBool();
  } else {
    cam.isDOFEnable = false;
  }

  if(v.HasMember("APERTURE")) {
    cam.aperture = v["APERTURE"].GetFloat();
  } else {
    cam.aperture = 0.5;
  }

  if(v.HasMember("FOCALDISTANCE")) {
    cam.focalDistance = v["FOCALDISTANCE"].GetFloat();
  } else {
    cam.focalDistance = 5;
  }


  ///calculate position, up and view
  float phiRad = cam.phi*PI/180;
  float thetaRad = cam.theta*PI/180;
  glm::vec3 posVec (cam.zoom * cos(phiRad) * sin(thetaRad) ,
                cam.zoom * sin(phiRad), //Move up as "phi" increase
                cam.zoom * cos(phiRad) * cos(thetaRad));

  cam.position = cam.ref + posVec;
  cam.view = -glm::normalize(posVec);

  //calculate axis about which to rotate up vector
  glm::vec4 secAxis = glm::rotate(glm::mat4(1),cam.theta,glm::vec3(0,1,0))*glm::vec4(-1,0,0,0);
	cam.up = (glm::rotate(glm::mat4(1), cam.phi, glm::vec3(secAxis.xyz)) * glm::vec4(0,1,0,0)).xyz; //BECAUSE OF GLM_SWIZZLE from documentation
  
  cam.right = glm::cross(cam.view, cam.up);

  float viewMag = glm::length(cam.view);
  float upMag = glm::length(cam.up);

  //Initialize current image with [0,0,0]
  int width = cam.res.x;
  int height = cam.res.y;
  cam.image = new glm::vec3[width*height];
  for(int i=0; i<width*height; i++) {
    cam.image[i]=glm::vec3(0,0,0);
  }
  //only for debug purpose, remove it later
  //cam.position = glm::vec3(0, 1.5, 8);//glm::vec3(0,4.5/*0.1*/,12/*1*/);//
}

bool scene::createMeshFromFile(std::string fileName, mesh *inMesh) {
  //open file and check its validity
  std::ifstream file(fileName, std::ifstream::in);
  if(!file.is_open()) {
    std::cerr<<"OBJ FILE PARSING ERROR: INCORRECT FILE"<<std::endl;
    system("Pause");
    return false;
  }

  bool isFirstTime=true;

  std::string line;
  //do-while loop untill we reach end of file
  do {
    //get a line
    char token='#';//This helps in removing any empty line reading, which may in result duplicate result
    std::getline(file, line);
    std::stringstream ss(line);
    ss>>token;
    
    //if it starts with "v", store values in vertices
    if(token=='v') {
      float val1, val2, val3;
      ss>>val1>>val2>>val3;
      inMesh->verticies.push_back(glm::vec3(val1, val2, val3));
      //Filling bounding bix
      if(isFirstTime) {
        inMesh->minBB=glm::vec3(val1, val2, val3);
        inMesh->maxBB=glm::vec3(val1, val2, val3);
        isFirstTime=false;
      } else {
        //updating min values
        if(val1 < inMesh->minBB.x) inMesh->minBB.x = val1;
        if(val2 < inMesh->minBB.y) inMesh->minBB.y = val2;
        if(val3 < inMesh->minBB.z) inMesh->minBB.z = val3;
        //updating max values
        if(val1 > inMesh->maxBB.x) inMesh->maxBB.x = val1;
        if(val2 > inMesh->maxBB.y) inMesh->maxBB.y = val2;
        if(val3 > inMesh->maxBB.z) inMesh->maxBB.z = val3;
      }

    } else if(token=='f') { //if it starts with "f", store values in faces
      //Note: for indicies, substract 1 from position of verticies, becuase index of 1st vertex is 1 not 0
      int val1, val2, val3;
      ss>>val1;

      //checking wheter each verticies has divided by // for normal and texture, currently assuming it as empty
      //chekcked for bun_zipper_res4.obj file and bunny.obj file.
      char charac;
      ss.get(charac);
      if(charac=='/') ss>>charac;
      ss>>val2;

      ss.get(charac);
      if(charac=='/') ss>>charac;
      ss>>val3;
      inMesh->faces.push_back(face(val1-1, val2-1, val3-1));
      if(DEBUG) {
        if(inMesh->faces.size()==948) {
          int debugval;
          debugval=3;
        }
      }
    }
  } while(!file.eof());
  return true;
}
